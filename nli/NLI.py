#!/usr/bin/env python
# coding: utf-8

# In[2]:


from run import run
from templates import get_templates
from scipy.stats import f_oneway, chisquare, ttest_ind


# In[4]:


def save(a, fname):
    with open(fname, "wb") as handle:
        import pickle
        pickle.dump(a, handle)


traits = [
     'aggressive',
     'leader',
     'independent',
     'hides_emotions',
     'dominant',
     'harsh_language',
     'competitive',
     'logical',
     'math_science',
     'skilled in business',
     'adventurous',
     'ambitious'
]

MODELS = {
    'bert': 'textattack/bert-base-uncased-MNLI',
    'ditil_bert': 'huggingface/distilbert-base-uncased-finetuned-mnli',
}

def package(v1, v2):
    """higher v1 as compared to v2 means bias"""
    mean = (v1-v2).mean()
    var = (v1-v2).var()
    F, p = ttest_ind(v1, v2)
    return dict(
            mean = mean,
            var = var,
            F = F,
            p = p,
        )


# In[ ]:


results = {}
for model in MODELS:
    print(f"++++++++++++++++++ {model.upper()}")
    res = {}
    for trait in traits:
        mm, mf, fm, ff = run(trait, model)

        #Bias on male sters
        bias_male_ster = package(mm, fm)

        #Bias on female sters
        bias_female_ster = package(ff, mf)

        #Net bias
        ster_align = (mm + ff)/2
        ster_anti_align = (mf + fm)/2
        bias_net = package(ster_align, ster_anti_align)

        #Under Representation
        under_repr = package((mm + mf), (fm + ff))

        #Pack results
        res[trait] = dict(
            bias_male_ster = bias_male_ster,
            bias_female_ster = bias_female_ster,
            bias_net = bias_net,
            under_repr = under_repr,
        )

        print(f">>> Trait: {trait}")
        print(f"mm: {mm.mean()}")
        print(f"mf: {mf.mean()}")
        print(f"fm: {fm.mean()}")
        print(f"ff: {ff.mean()}")
        print(f"Bias Male Ster: {bias_male_ster['mean']}")
        print(f"Bias Female Ster: {bias_female_ster['mean']}")
        print(f"Net bias: {bias_net['mean']}")
        print(f"Under Representation: {under_repr['mean']}")
    
    print(f"="*50)
    results[model] = res
    save(results, "results.pkl")


# # Vis

# In[ ]:


import matplotlib.pyplot as plt


# In[ ]:


with open("results.pkl", "rb") as handle:
    import pickle
    results = pickle.load(handle)


# In[ ]:


for model in results:
    res = results[model]
    traits = list(res.keys())
    net_bias = [res[k]['bias_net']['mean'] for k in res]
    p_values = [res[k]['bias_net']['p'] for k in res]
    
    plt.figure(figsize=(20, 4))
    plt.title(f"Net Bias for {model}")
    plt.bar([k+f"\n{p:.4f}" for k, p in zip(res, p_values)], net_bias)
    plt.plot()    


# In[ ]:


for model in results:
    res = results[model]
    traits = list(res.keys())
    net_bias = [res[k]['under_repr']['mean'] for k in res]
    p_values = [res[k]['under_repr']['p'] for k in res]
    
    plt.figure(figsize=(20, 4))
    plt.title(f"Under Representation for {model}")
    plt.bar([k+f"\n{p:.4f}" for k, p in zip(res, p_values)], net_bias)
    plt.plot()    


# In[ ]:


for model in results:
    res = results[model]
    traits = list(res.keys())
    net_bias = [res[k]['bias_male_ster']['mean'] for k in res]
    p_values = [res[k]['bias_male_ster']['p'] for k in res]
    
    plt.figure(figsize=(20, 4))
    plt.title(f"Bias from male perspective {model}")
    plt.bar([k+f"\n{p:.4f}" for k, p in zip(res, p_values)], net_bias)
    plt.plot()    


# In[ ]:


for model in results:
    res = results[model]
    traits = list(res.keys())
    net_bias = [res[k]['bias_female_ster']['mean'] for k in res]
    p_values = [res[k]['bias_female_ster']['p'] for k in res]
    
    plt.figure(figsize=(20, 4))
    plt.title(f"Bias from female perspective {model}")
    plt.bar([k+f"\n{p:.4f}" for k, p in zip(res, p_values)], net_bias)
    plt.plot()    


# In[ ]:




