import dataclasses
import logging
import os
import glob
import sys
import time
from dataclasses import dataclass, field
from typing import Dict, Optional

import numpy as np

from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer, EvalPrediction, GlueDataset
from transformers import GlueDataTrainingArguments as DataTrainingArguments
from transformers import (
    HfArgumentParser,
    Trainer,
    TrainingArguments,
    glue_compute_metrics,  #, working for now
    glue_output_modes,
    glue_tasks_num_labels,
    set_seed,
)
# import transformers.data.metrics.__init__ as metrics # hack if prior import gives error
from scipy.special import softmax

logger = logging.getLogger()
logger.disabled = True #Disable logging


global_task = ""

def accuracy(a, b):
    """was verifying, useless now"""
    assert len(a) == len(b)
    return (a==b).sum()/len(a)

def compute_metrics(p: EvalPrediction) -> Dict:
    preds = np.argmax(p.predictions, axis=1)
    global global_task 
    return glue_compute_metrics(global_task, preds, p.label_ids)

class Evaluator():
    """basically a clean wrapper around what tenzin did last week.
    todo: add other models (maybe later, for now testing 
    bert-base-uncased).
    """
    def __init__(self, model_name="textattack/bert-base-uncased-MNLI",
                task_name="MNLI"):
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.model = AutoModelForSequenceClassification.from_pretrained(
                    model_name)
        self.task_name = task_name           

    def flush_cache(self, data_dir):
        """wasted so much time in debugging this :facepalm:
        if cache not cleared, model won't evaluate again so clear cache
        on every run. Might be a good thing sometimes tho.
        `flush_cash` seems a bad idea xD.
        """
        cached_files = glob.glob(os.path.join(data_dir, "cached*"))
        n = len(cached_files)
        [os.remove(f) for f in cached_files]
        return n    
    
    def run(self, data_dir="data/", output_dir=".", bs=128, logits=False,
            clear_cache=True):
        global global_task
        if clear_cache:
            n = self.flush_cache(data_dir)
            # print(f"Flushed {n} cached files.")
        data_args = DataTrainingArguments(
                        task_name=self.task_name, 
                        data_dir=data_dir
        )
        eval_args = TrainingArguments(
            output_dir=output_dir,
            do_train=False,
            do_eval=True,
            per_gpu_eval_batch_size=bs,
        )
        eval_dataset = GlueDataset(data_args, tokenizer=self.tokenizer, 
                                mode='dev')
        trainer = Trainer(
                model=self.model,
                args=eval_args,
                eval_dataset=eval_dataset,
                compute_metrics=compute_metrics,
        )  
        global_task = data_args.task_name
        preds, labels, metrics = trainer.predict(eval_dataset)
        if logits:
            return softmax(preds, axis=1), preds.argmax(axis=1), labels, metrics    
        return preds.argmax(axis=1), labels, metrics