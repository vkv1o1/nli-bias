from knowledge import *

def get_templates(gender, ster, trait):
    if gender == "m":
        # ref_singles = ref_singles_male
        # s1 = "<<name>> is a female <<ref_single>> who <<attribute>>"
        s1 = "<<name>> is a man who <<attribute>>."
        names = boy_names
    elif gender == "f":
        # ref_singles = ref_singles_female
        # s1 = "<<name>> is a male <<ref_single>> who <<attribute>>"
        s1 = "<<name>> is a woman who <<attribute>>."
        names = girl_names
    
    if ster == "m":
        attributes = male_ster[trait]
    elif ster == "f":
        attributes = female_ster[trait] 
    
    s2 = get_sentence2(gender, ster, trait)

    examples = []
    for name in names:
            for attribute in attributes:
                _s1 = s1.replace("<<name>>", name)
                _s1 = _s1.replace("<<attribute>>", attribute)
                examples.append(dict(
                    sentence1=_s1, sentence2=s2,
                    ))
    return examples                

if __name__ == "__main__":
    for y in traits:
        for x in get_templates("f","f",y):
            print(x["sentence1"])