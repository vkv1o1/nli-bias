from checklist.editor import Editor
import argparse
import pickle
import numpy as np

editor = Editor()
editor_prtb = {'syn': editor.synonyms,
               'ant': editor.antonyms,
               'hpr': editor.hypernyms,
               'hpo': editor.hyponyms}

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def infer_results(res, mname):
    print(f">>> {mname}")
    differences = []
    for i, k in enumerate(res):
        if not i:
            continue
        for m in res[k]:
            acc_g, acc_b = res[k][m][0], res[k][m][1]
            differences.append(acc_b - acc_g)
    ress = np.array(differences)
    print("Biased: ", (ress > 0).sum()/len(ress), f" ({(ress > 0).sum()})")
    print("Rev-Biased: ", (ress < 0).sum()/len(ress), f" ({(ress < 0).sum()})")
    print("Biased .1: ", (ress > 0.1).sum()/len(ress), f" ({(ress > 0.1).sum()})")
    print("Rev-Biased .1: ", (ress < -0.1).sum()/len(ress), f" ({(ress < -0.1).sum()})")
    print(f"[Avg. Biased Templates diff]: {ress[ress>0].mean()}")
    return ress, (ress > 0).sum()/len(ress), (ress < 0).sum()/len(ress)

def save_pickle(obj, fname):
    with open(fname, "wb") as handle:
        pickle.dump(obj, handle)                

def load(fname):
    with open(fname, "rb") as handle:
        return pickle.load(handle)

# if True:
#     for i, k in enumerate(bert_res):
#         if not i:
#             continue
#         for m in bert_res[k]:
#             acc_g, acc_b = bert_res[k][m][0], bert_res[k][m][1]
#         if acc_b - acc_g < -0.1:
#             print(k)