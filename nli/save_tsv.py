import os
import numpy as np
import pandas as pd

COLS = ['index', 'promptID', 'pairID', 'genre', 
        'sentence1_binary_parse', 'sentence2_binary_parse', 
        'sentence1_parse', 'sentence2_parse', 'sentence1', 
        'sentence2', 'label1', 'label2', 'label3', 'label4', 
        'label5', 'gold_label']

def template_to_tsv(checklist_data, label, fname, dirname="data/", ret_df=False):
    
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    df = pd.DataFrame()
    l = len(checklist_data)
    nas = [np.nan for _ in range(l)]
    indices = [i for i in range(l)]
    cols = ['promptID', 'pairID', 'genre', 'sentence1_binary_parse',
       'sentence2_binary_parse', 'sentence1_parse', 'sentence2_parse',
       'label1', 'label2', 'label3', 'label4', 'label5']
    
    for col in cols:
        df[col] = nas

    df['index'] = indices
    df['sentence1'] = [instance['sentence1'] for instance in checklist_data]    
    df['sentence2'] = [instance['sentence2'] for instance in checklist_data]    
    if isinstance(label, list):    
        df['gold_label'] = label
    else:
        df['gold_label'] = [label for _ in range(l)]
    df = df[COLS] #Order matters    
    
    if not os.path.exists(dirname):
        os.mkdir(dirname)
        
    path = os.path.join(dirname, fname)
    df.to_csv(path, index=False, sep='\t')
    # print(f"Saved data to {path} (Size: {df.shape})")
    return dirname