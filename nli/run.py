import sys
try:
    import pattern
except:    
    sys.path.append("/home/timetraveller/Desktop/pattern/")
# import checklist
# from checklist.editor import Editor
from checklist.perturb import Perturb
from save_tsv import template_to_tsv
from evaluate_nli import Evaluator
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import seaborn as sns
import logging 
import argparse
import pickle
from scipy.stats import f_oneway, chisquare
import statsmodels.stats.proportion as stats
import logging
import re
from utils import editor

from templates import get_templates

# Logging utils
def set_global_logging_level(level=logging.ERROR, prefices=[""]):
    """
    Override logging levels of different modules based on their name as a prefix.
    It needs to be invoked after the modules have been loaded so that their loggers have been initialized.

    Args:
        - level: desired level. e.g. logging.INFO. Optional. Default is logging.ERROR
        - prefices: list of one or more str prefices to match (e.g. ["transformers", "torch"]). Optional.
          Default is `[""]` to match all active loggers.
          The match is a case-sensitive `module_name.startswith(prefix)`
    """
    prefix_re = re.compile(fr'^(?:{ "|".join(prefices) })')
    for name in logging.root.manager.loggerDict:
        if re.match(prefix_re, name):
            logging.getLogger(name).setLevel(level)

set_global_logging_level(logging.ERROR, ["transformers", "nlp", 
                                        "torch", "tensorflow", 
                                        "tensorboard", "wandb"])
logging.disable(logging.INFO)

MODELS = {
    'bert': 'textattack/bert-base-uncased-MNLI',
    'distil_bert': 'huggingface/distilbert-base-uncased-finetuned-mnli',
    'roberta': "cross-encoder/nli-roberta-base",
    'distil_roberta': "cross-encoder/nli-distilroberta-base",
}

def get_model_name(model):
    return MODELS[model]

def get_models(model_name):
    model_name = get_model_name(model_name)
    return Evaluator(model_name=model_name)

def run(trait, model_name="bert", label="entailment"):
    # template_data =  #list of dict with s1 and s2 keys
    # template_data, label = get_templates(ttype)
    evaluator = get_models(model_name)
    results = []
    for gender, ster in ["mm", "mf", "fm", "ff"]:
        template_data = get_templates(gender, ster, trait)
        
        # print(f"{gender}-{ster}")
        # for x in template_data[:5]:
        #     print(x)

        data_dir = template_to_tsv(template_data, label, 'dev_matched.tsv', 'data/')
        logits, _, _, _ = evaluator.run(data_dir, logits=True)
        logits = logits[:, 1]
        results.append(logits)
    return results 

# LEGEND:
# 0: contradiction
# 1: entailment
# 2: neutral