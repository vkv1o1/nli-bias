import pickle



def get_occupations(N=100):
    male_occ = """janitor
    banker
    farmer
    lawyer
    manager
    pilot
    laborer
    construction worker
    developer
    carpenter
    mechanic
    doctor
    engineer
    surgeon
    boss
    sheriff
    CEO""".split("\n")

    female_occ = """attendant
    cashier
    teacher
    nurse
    assistant
    secretary
    auditor
    receptionist
    clerk
    counselor
    hairdresser
    writer
    housekeeper
    baker
    accountant
    editor
    librarian
    tailor""".split("\n")

    def fix(l, n):
        return [x.strip() for x in l if len(x.strip())>0][:n]

    if len(male_occ) > len(female_occ):
        l = len(male_occ)
    else:
        l = len(female_occ) 

    male_occ = fix(male_occ, l)    
    female_occ = fix(female_occ, l)    
    
    if N > len(male_occ):
        print(f"Only {len(male_occ)} occupations.")
    return female_occ[:N], male_occ[:N] 

def _names():
    fem_names = """Olivia
    Emma
    Ava
    Sophia
    Isabella
    Charlotte
    Amelia
    Mia
    Harper
    Evelyn
    Abigail
    Emily
    Ella
    Elizabeth
    Camila
    Luna1
    Sofia
    Avery
    Mila
    Aria
    Scarlett
    Penelope
    Layla
    Chloe
    Victoria
    Madison
    Eleanor
    Grace
    Nora
    Riley
    Zoey
    Hannah
    Hazel
    Lily
    Ellie
    Violet
    Lillian
    Zoe
    Stella
    Aurora
    Natalie
    Emilia
    Everly
    Leah
    Aubrey
    Willow
    Addison
    Lucy
    Audrey
    Bella
    Nova
    Brooklyn
    Paisley
    Savannah
    Claire
    Skylar
    Isla
    Genesis
    Naomi
    Elena
    Caroline6
    Eliana
    Anna
    Maya
    Valentina
    Ruby
    Kennedy
    Ivy
    Ariana
    Aaliyah
    Cora
    Madelyn
    Alice
    Kinsley
    Hailey
    Gabriella
    Allison
    Gianna
    Serenity
    Samantha
    Sarah
    Autumn
    Quinn
    Eva
    Piper
    Sophie
    Sadie
    Delilah
    Josephine
    Nevaeh
    Adeline
    Arya
    Emery
    Lydia
    Clara
    Vivian
    Madeline
    Peyton
    Julia
    Rylee""".split('\n')    


    male_names = """Liam
    Noah
    William
    James
    Oliver
    Benjamin
    Elijah
    Lucas
    Mason
    Logan
    Alexander
    Ethan
    Jacob
    Michael
    Daniel
    Henry
    Jackson
    Sebastian
    Aiden
    Matthew
    Samuel
    David
    Joseph
    Carter
    Owen
    Wyatt
    John
    Jack
    Luke
    Jayden
    Dylan
    Grayson
    Levi
    Isaac
    Gabriel
    Julian
    Mateo
    Anthony
    Jaxon
    Lincoln
    Joshua
    Christopher
    Andrew
    Theodore
    Caleb
    Ryan
    Asher
    Nathan
    Thomas
    Leo
    Isaiah
    Charles
    Josiah
    Hudson
    Christian
    Hunter
    Connor
    Eli
    Ezra
    Aaron
    Landon
    Adrian
    Jonathan
    Nolan
    Jeremiah
    Easton
    Elias
    Colton
    Cameron
    Carson
    Robert
    Angel
    Maverick
    Nicholas
    Dominic
    Jaxson
    Greyson
    Adam
    Ian
    Austin
    Santiago
    Jordan
    Cooper
    Brayden
    Roman
    Evan
    Ezekiel
    Xavier
    Jose
    Jace
    Jameson
    Leonardo
    Bryson
    Axel
    Everett
    Parker
    Kayden
    Miles
    Sawyer
    Jason""".split('\n')

    return fem_names, male_names

def get_names(N=100, names_only=True):
    """returns N [girl names, boy names] each"""
    def fix(l, N):
        return [x.strip() for x in l][:N]

    fem, male = _names()
    fem = fix(fem, N)
    male = fix(male, N)

    return fem, male                      


