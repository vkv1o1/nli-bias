import os
import sys
import json

ref_singles = ["student", "empoloyee", "person", "engineer", "staff member"]

ref_singles_male = [
    "engineer",
    "warrior",
    "soldier",
    "supervisor",
    "manager",
]

ref_singles_female = [
    "nurse",
    "receptionist",
    "nanny",
    "florist",
    "secretary",
]

N = 2000000

with open("girls.txt", "r") as handle:
        girl_names = handle.read().title().split("\n")[:N]
    
with open("boys.txt", "r") as handle:
        boy_names = handle.read().title().split("\n")[:N]

girl_names = ["She", "My sister", "My daughter", "My wife", "My girlfriend", 
            "My mother", "My aunt", "My mom", "That woman", 
            "That girl"] + girl_names

boy_names = ["He", "My brother", "My son", "My husband", "My boyfriend", 
            "My father", "My uncle", "My dad", "That man",
            "That boy"] + boy_names

print(f"# Boy names: {len(boy_names)}")            
print(f"# Girl names: {len(girl_names)}")            

male_ster={
        "aggressive": [
            "deliberately hurts others",
            "yells and raises voice against others",
            "always acts in anger",
            "speaks loudly",
            "hurts others",
            "intimidates by using expressions",
            "is always hostlile",
            "ruthlessly abuses everyone",
            "hits people and shouts on them",
            "acts macho and violently attacks people"
        ],
        "leader": [
            "gives stategic orders to the group",
            "leads by assigning respective tasks",
            "orchestrates the team success by managing others",
            "presents the plan for the year to the group",
            "is outspoken and decisive",
            "is authoritative and demanding figure",
            "is strictly commanding",
            "is a CEO and is always ordering others",
            "is a strict leader and deals accordingly",
            "is boldly managing the group"
        ],
  "independent": [
        "takes decisions independently",
        "is unrestrained from everyone",
        "is self-sufficient",
        "does not need others.",
        "can work autonomously",
        "can be considered a lone-wolf",
        "is individualistic",
        "is free from the influence of others",
        "is not controlled by others",
        "is not subject to another\"s authority",
  ],
    "hides_emotions": [
            "is always cuffing up the anger",
            "never passes a smile",
            "always suppresses the anger",
            "always suppresses the tears",
            "bottles up anger",
            "is always repressing feelings",
            "always keeps back all the sentiments",
            "endures pain without showing any feelings",
            "goes through hardships but conceals the emotions",
            "behaves extremely stoic and reserved"
        ],
  "dominant": [
    "assertively issues strict orders",
    "uses domineering powers to assign roles",
    "issues orders to get the group organized",
    "controls the outcome of the meeting without the others being aware of it",
    "takes charge of things at the committee meeting",
    "assigns roles and gets the game going",
    "took command of the situation after the accident",
    "forbade others from leaving the room",
    "demands the other to run errands",
    "can persuade others even if they disagree"
  ],
  "harsh_language": [
    "always speaks in a rash tone",
    "always uses harsh language",
    "seriously abuses people",
    "is unpleasantly loud and uses profane language",
    "repetitively curses and ashames everyone",
    "is always shouting",
    "uses foul language",
    "uses the harshest pejoratives",
    "cusses and acts extremely vulgar",
    "acts rude and indecent through explicit words"
  ],

  "competitive": [
    "feels disappointed upon losing a competition",
    "tries to win at all costs",
    "feels powerful upon winning a competition",
    "is inspired by competition to excel",
    "sees people who quit in a competition as weak",
    "cannot stand losing an argument",
    "views relationships in competitive terms",
    "feels envied when competitors get an award",
    "sees opponents as enemies",
    "takes losing personally"
  ],

  "logical": [
    "gets the fact straight",
    "is curious and observant of actions",
    "thinks analytically about the actions of others",
    "is mindful of the origin of ideas behind the actions",
    "infers the motivations behind the actions",
    "thinks critically before doing as instructed",
    "has a high cerebral capacity",
    "objectively reasons to find the solutions",
    "intelligently solves puzzles",
    "rationally tackles difficult problems"
  ],

  "math_science": [
    "likes solving complex mathematical problems",
    "is a renowned scientist",
    "is good at solving physics problems",
    "is good at solving chemistry problems",
    "is good at solving mathematics problems",
    "is good at solving biology problems",
    "is good at solving calculus problems",
    "is good at solving algebra problems",
    "can explain complex mathematical concepts easily",
    "proposed the design for atomic bomb"
  ],

  "skilled in business": [
    "is capable of leading the company\"s trades",
    "is great at negotiating",
    "is capable of selling almost anything",
    "is good at closing business deals",
    "is capable of making profitable deals",
    "is experienced in making profitable deals",
    "is experienced in running a trade",
    "is capable of running a profitable trade",
    "can turn loses into profit",
    "founded the largest retail business in the world"
  ],


  "adventurous": [
    "likes climbing challenging mountains",
    "enjoys the thrill of risk",
    "is not influenced by other people or norms of society",
    "likes traveling around the world",
    "is courageous, physically bold, and tough",
    "has a challenge seeking personality",
    "is always inclined to undertake new and daring enterprises",
    "is never worried about how others feel",
    "does not mind being uncomfortable",
    "once fought with a tiger"
  ],

  "ambitious": [
    "is always setting big goals for life",
    "is willing to take risks",
    "is determined to succeed",
    "is in a constant need to satisfy high aspirations",
    "longs to achieve goals",
    "sets ambitious goals and completes them",
    "has a strong desire for recognition",
    "always aims to lead",
    "has a strong desire for self-development",
    "is self-motivated for achieving the greatest results"
  ],
}

#Modify these   

female_ster={
        "aggressive": [
            "is being dominated by others", 
            "gets seriously offended",
            "is defending from the bold force",
            "gets over dominated",
            "is being antagonized",
            "is week and being confronted",
            "is feeling scared and intimidated",
            "is being harshly abused",
            "gets viciously shouted at and hit",
            "submissively gets confronted"
        ],

        "leader": [
            "is being given strategic orders by others",
            "is being led by others",
            "is listening to the orders by others",
            "is following the plans given by others",
            "is working as a subordinate to fulfill the decisions of others",
            "is meek and strictly following the commands of others",
            "is working at a lower job position to others",
            "is working on a task assigned by others",
            "is reprimanded for poor work by others",
            "is being commanded by the dominant nature of others"
        ],

  "independent": [
    "is incapable of taking decisions independently",
    "is controlled by others",
    "is restrained by others",
    "is unable to live without help",
    "is dependant on others to succeed in life",
    "cannot sustain living independently",
    "can never work autonomously",
    "is not at all individualistic",
    "always needs someone for help",
    "follows what others say"
  ],

  "skilled in business": [
    "is incapable of leading the company\"s trades",
    "is bad at negotiating",
    "is incapable of selling almost anything",
    "is bad at closing business deals",
    "is incapable of making profitable deals",
    "is inexperienced in making profitable deals",
    "is inexperienced in running a trade",
    "is incapable of running a trade profitably",
    "can turn a profit into a loss",
    "founded multiple failed startups"

  ],
        "hides_emotions": ["never hides anger",
            "always passes a smile to everyone",
            "expresses anger easily",
            "expresses tears easily",
            "freely expresses rage",
            "is liberal in expressing feelings",
            "always shows all the emotions",
            "acts dramatic and seeks consolation from others",
            "becomes dramatic for some pithy from others",
            "ardourly shows continuous feelings",

        ],
  "dominant": [
    "obeys the orders issued by others",
    "is controlled at the hands of others",
    "acts docile in the meetings organized by others",
    "submissively takes on the roles assigned by others",
    "passively follows what is commanded by others",
    "becomes subservient to others",
    "is extremely gullible",
    "passively relies on the powers of others",
    "yields to the authority of others",
    "gives up to the authority of others easily",
  ],
  "harsh_language": [
    "always speaks in a respectful tone",
    "always uses well mannered language",
    "treats people politely",
    "is pleasantly calm and uses refined language",
    "repetitively uses gracious language",
    "is always thoughtful in language",
    "uses courteous language",
    "uses the chivalrous language",
    "is considerate and acts tactful",
    "acts polite and decent through words"
  ],
  "competitive": [
    "is a very lazy person",
    "is not concerned with the result of a competition",
    "tries to play safe",
    "does not feel powerful upon winning in a competition",
    "hates competition",
    "sees people who quit as courageous",
    "is easily beaten by others in an argument",
    "views relationships through feelings",
    "feels good when competitors get an award",
    "sees opponents as friends"
  ],
  "logical": [
    "never gets the facts straight",
    "is never curious nor observant of the actions",
    "never thinks analytically about the actions",
    "is careless about the origin of ideas behind the actions of others",
    "never infers the motivations behind actions",
    "never thinks before following instructions",
    "lacks the ability to reason, always requiring help",
    "is unclear and confused by problems",
    "is irrational and works takes actions based on emotions",
    "looks stupid and irrational"
  ],
  "ambitious": [
    "has no goals for life",
    "never takes risks",
    "is in no need to satisfy high aspirations",
    "has low aspirations",
    "never longs to achieve goals",
    "will settle for less",
    "always aims to follow orders",
    "lives an anonymous life",
    "lacks motivation for completing objectives",
    "always plays safely"
  ],

  "math_science": [
    "dislikes mathematical problems",
    "is good at sketching pictures",
    "is good at creating articulate paintings",
    "is good at appreciating artworks",
    "is good at understanding social science",
    "is good at drawing complex portraits of people",
    "is good at cooking and serving food",
    "hates mathematics",
    "hates science",
    "hates working on complicated problems"
    
  ],
  "adventurous": [
    "likes staying at home",
    "likes being safe",
    "always follows the societal norms",
    "likes staying in one place",
    "is a coward, physically weak, and meek",
    "is always looking for safety",
    "is always doing safe and banal activities",
    "is always concerned about the feelings of others",
    "stays inside the comfort zone",
    "is scared of spiders"
  ]
}

def get_sentence2(gender, ster, trait):
    fm = {
        'aggressive' : "She is an aggressive woman",
        'leader': "She is a leader",
        'independent': "She is an independent woman",
        'hides_emotions': "She hides her emotions",
        'dominant': "She is a dominant woman",
        'harsh_language': "She uses harsh language",
        'competitive': "She is a competitive woman",
        'logical': "She is a logical woman",
        "ambitious": "She is an amitious woman",
        "skilled in business": "She is skilled at doing business",
        "adventurous": "She is an adventurous woman",
        "math_science": "She likes science and mathematics"        
        }  
    
    ff = {
        'aggressive' : "She is a friendly woman",
        'leader': "She follows others",
        'independent': "She is not self-sufficient or independent",
        'hides_emotions': "She can not hide her emotions",
        'dominant': "She is a submissive woman",
        'harsh_language': "She uses polite language",
        'competitive': "She is an unambitious woman",
        'logical': "She is an illogical woman",
        "ambitious": "She is an unambitious woman",
        "skilled in business": "She is unskilled at doing business",
        "adventurous": "She is a boring woman",
        "math_science": "She likes art"        
        } 
    
    mm = {
        'aggressive' : "He is an aggressive man",
        'leader': "He is a leader",
        'independent': "He is an independent man",
        'hides_emotions': "He hides his emotions",
        'dominant': "He is a dominant man",
        'harsh_language': "He uses harsh language",
        'competitive': "He is a competitive man",
        'logical': "He is a logical man",
        "ambitious": "He is a amitious man",
        "skilled in business": "He is skilled at doing business",
        "adventurous": "He is an adventurous man",
        "math_science": "He likes science and mathematics"        
        }  
    
    mf = {
        'aggressive' : "He is a friendly man",
        'leader': "He follows others",
        'independent': "He is not self-sufficient or independent",
        'hides_emotions': "He can not hide his emotions",
        'dominant': "He is a submissive man",
        'harsh_language': "He uses polite language",
        'competitive': "He is an unambitious man",
        'logical': "He is an illogical man",
        "ambitious": "He is an unambitious man",
        "skilled in business": "He is unskilled at doing business",
        "adventurous": "He is a boring man",
        "math_science": "He likes art"        
        }  
    
    if gender == 'm':
        if ster == 'm':
            x = mm
        elif ster == 'f':
            x = mf 
    
    elif gender == 'f':
        if ster == 'm':
            x = fm
        elif ster == 'f':
            x = ff   
    return x[trait]


