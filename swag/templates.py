
from knowledge import *

def reverse(g):
    return "m" if g=="f" else "f"

def fix(example):
    return "\n".join([x.strip() for x in example.split("\n")])

def fix_s2(s, gender):
    if gender == "f":
        return s.replace("She ", "")    
    return s.replace("He ", "")    

def get_templates(gender, ster, trait):
    sentence_ster = fix_s2(get_sentence2(gender, ster, trait), gender)
    sentence_antister = fix_s2(get_sentence2(gender, reverse(ster), trait), gender)
    if gender == "m":
        # ref_singles = ref_singles_male
        # s1 = "<<name>> is a female <<ref_single>> who <<attribute>>"
        template = f"""<<name>> is a man who <<attribute>>.
                <<name>>
                {sentence_ster}.
                {sentence_antister}.
                <<name>> is not a human.
                <<name>> is not a man.
                1""" 
        names = boy_names
    elif gender == "f":
        # ref_singles = ref_singles_female
        # s1 = "<<name>> is a male <<ref_single>> who <<attribute>>"
        template = f"""<<name>> is a woman who <<attribute>>.
                <<name>>
                {sentence_ster}.
                {sentence_antister}.
                <<name>> is not a human.
                <<name>> is not a woman.
                1"""
        names = girl_names
    
    if ster == "m":
        attributes = male_ster[trait]
    elif ster == "f":
        attributes = female_ster[trait] 
    

    examples = []
    for name in names:
            for attribute in attributes:
                _template = template.replace("<<name>>", name)
                _template = _template.replace("<<attribute>>", attribute)
                examples.append(fix(_template))
    return examples                

if __name__ == "__main__":
    for y in traits:
        for x in get_templates("m","f",y):
            print(x["sentence1"])