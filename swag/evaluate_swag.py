import dataclasses
import logging
import os
import glob
import sys
import time
from dataclasses import dataclass, field
from typing import Dict, Optional

import numpy as np

# from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer, EvalPrediction, GlueDataset
# from transformers import GlueDataTrainingArguments as DataTrainingArguments
# from transformers import (
#     HfArgumentParser,
#     Trainer,
#     TrainingArguments,
# #     glue_compute_metrics, weird not importing
#     # glue_output_modes,
#     # glue_tasks_num_labels,
#     set_seed,
# )
from transformers import (
    AutoConfig,
    AutoModelForMultipleChoice,
    AutoTokenizer,
    EvalPrediction,
    HfArgumentParser,
    Trainer,
    TrainingArguments,
    set_seed,
)
# import transformers.data.metrics.__init__ as metrics #hack for pior import error

logger = logging.getLogger()
logger.disabled = True #Disable logging

from utils_multiple_choice import MultipleChoiceDataset, Split

def simple_accuracy(preds, labels):
    return (preds == labels).mean()

def compute_metrics(p: EvalPrediction) -> Dict:
    preds = np.argmax(p.predictions, axis=1)
    return {"acc": simple_accuracy(preds, p.label_ids)}

class Evaluator():
    """basically a clean wrapper around what tenzin did last week.
    todo: add other models (maybe later, for now testing 
    bert-base-uncased).
    """
    def __init__(self, model_name="textattack/bert-base-uncased-MNLI",
                task_name="swag"):
        config = AutoConfig.from_pretrained(model_name, num_labels=4,
        finetuning_task=task_name,
        )
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.model = AutoModelForMultipleChoice.from_pretrained(
                    model_name, config=config)
        self.task_name = task_name           

    def flush_cache(self, data_dir):
        """wasted so much time in debugging this :facepalm:
        if cache not cleared, model won't evaluate again so clear cache
        on every run. Might be a good thing sometimes tho.
        `flush_cash` seems a bad idea xD.
        """
        cached_files = glob.glob(os.path.join(data_dir, "cached*"))
        n = len(cached_files)
        # print(cached_files)
        [os.remove(f) for f in cached_files]
        return n    
    
    def run(self, data_dir="data/templates", output_dir=".", bs=128, max_len = 80, pred=False,
            clear_cache=True, prob=False):
        if clear_cache:
            n = self.flush_cache(data_dir)
            print(f"Flushed {n} cached files.")
        # data_args = DataTrainingArguments(
        #                 task_name=self.task_name, 
        #                 data_dir=data_dir
        # )
        eval_args = TrainingArguments(
            output_dir=output_dir,
            do_train=False,
            do_eval=True,
            per_device_eval_batch_size=bs,
        )
        eval_dataset = MultipleChoiceDataset(
            data_dir=data_dir,
            tokenizer=self.tokenizer,
            task=self.task_name,
            max_seq_length=max_len,
            overwrite_cache=False,
            mode=Split.dev,
        )
        
        
        # GlueDataset(data_args, tokenizer=self.tokenizer, 
        #                         mode='dev')
        trainer = Trainer(
                model=self.model,
                args=eval_args,
                eval_dataset=eval_dataset,
                compute_metrics=compute_metrics,
        )  
        # global_task = data_args.task_name
        preds, label, metrics = trainer.predict(eval_dataset)
        
        if prob and pred:
            print(f"warning both prob and pred are true")
            preds = preds
        elif prob: #logits to prob
            preds = np.exp(preds)/(np.sum(np.exp(preds), axis=1)[:, np.newaxis])
        elif pred: #logits might be useful for differential study
            preds = preds.argmax(axis=1)
        
        return preds, label, metrics