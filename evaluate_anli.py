from transformers import AutoTokenizer, AutoConfig, AutoModelForMultipleChoice
from anli.data_processors import AnliProcessor, mc_examples_to_data_loader# get_examples_from_file
import json
import torch
from tqdm import tqdm
import numpy as np
from checklist.editor import Editor
import matplotlib.pyplot as plt
from get_templates import *


def evaluate(fname, bs=8, 
    model_dir = 'models/anli/xlnet-ft-lr1e-5-batch8-epoch4-maxlen100/', 
    ret_confidence=False):
    
    config = AutoConfig.from_pretrained(model_dir, num_labels = 2)
    model = AutoModelForMultipleChoice.from_pretrained(model_dir, 
                config=config)
    device = torch.device('cuda')
    model.to(device)
    model.eval()

    data_processor = AnliProcessor()
    pred_examples = data_processor.get_examples_from_file(fname)

    tokenizer = AutoTokenizer.from_pretrained(model_dir, 
                                              do_lower_case=True)
    pred_dataloader = mc_examples_to_data_loader(examples=pred_examples,
                                                 tokenizer=tokenizer,
                                                 max_seq_length=68,
                                                 is_train=False,
                                                 is_predict=True,
                                                 batch_size=bs,
                                                 verbose=False
                                                 )
    pred_labels = []
    preds_list = []
    for input_ids, input_mask, segment_ids in tqdm(pred_dataloader):
        input_ids = input_ids.to(device)
        input_mask = input_mask.to(device)
        segment_ids = segment_ids.to(device)

        with torch.no_grad():
            if 'roberta' in model_dir:
                model_output = model(input_ids=input_ids, 
                                     attention_mask=input_mask)
            else:
                model_output = model(input_ids=input_ids, 
                                     token_type_ids=segment_ids, 
                                     attention_mask=input_mask)
            logits = model_output[0]

        preds = torch.softmax(logits, dim=1).detach().cpu().numpy()
        preds_list.extend(preds)
    #     print(preds)
        pred_labels.extend(np.argmax(preds, axis=1).tolist())   
    if ret_confidence:    
        return pred_labels, preds_list    
    return pred_labels 


def to_json(examples):
    json_data = []
    for id, example in enumerate(examples):
        o1, o2, h1, h2 = example.split('\n')
        d = dict(
            story_id = id,
            obs1 = o1,
            obs2 = o2,
            hyp1 = h1,
            hyp2 = h2,
        )
        json_data.append(d)
    return json_data

def infer(pred_labels, confidence_levels=None, verbose=False):
    if confidence_levels is not None:
        h1_c = np.array(confidence_levels)[:, 0]
        h2_c = np.array(confidence_levels)[:, 1]

        diff_mean = (h1_c - h2_c).mean()
        diff_var = (h1_c - h2_c).var()
         
        h1_c = h1_c[h1_c>0.5]
        h2_c = h2_c[h2_c>0.5]
        
        confidence_h1_mean = h1_c.mean()
        confidence_h2_mean = h2_c.mean()
        
        confidence_h1_var = h1_c.var()
        confidence_h2_var = h2_c.var()        
        
        
    
    pred_labels = np.array(pred_labels)
    l = len(pred_labels)
    l1 = (pred_labels==0).sum()/len(pred_labels)
    l2 = (pred_labels==1).sum()/len(pred_labels)    
    avg = pred_labels.mean()
    if verbose:
        print(f"%hyp1: {l1}")
        print(f"%hyp2: {l2}")    
        print(f"avg: {avg}")   
    data = [l1, l2, avg]
    if confidence_levels is not None:
        if verbose:
            print(f"confidence_h1_mean: {confidence_h1_mean}")
            print(f"confidence_h1_var: {confidence_h1_var}")
            print(f"confidence_h2_mean: {confidence_h2_mean}")
            print(f"confidence_h2_var: {confidence_h2_var}") 
            print(f"diff_mean: {diff_mean}")
            print(f"diff_var: {diff_var}")                
        data.append(confidence_h1_mean)
        data.append(confidence_h1_var)
        data.append(confidence_h2_mean)
        data.append(confidence_h2_var)   
        data.append(l)     
        data.append(diff_mean)     
        data.append(diff_var)     
    return data

def run(model_dir, gender='m', ster='m', ttype=1, reverse=False):
    combined_data = {}
    confidences = {}
    traits, refs_multiple, refs_single, males_ref, females_ref = get_lists()

    for trait in traits:
        if ttype == 1:
            #This type is about guessing the trait given the gender
            if gender == 'm':
                if ster == 'm':
                    print("template_mm")
                    template = template_mm(trait, reverse)
                elif ster == 'f':
                    print("template_mf")
                    template = template_mf(trait, reverse) 
                else:
                    raise Exception("what??")

            elif gender == 'f':
                if ster == 'm':
                    print("template_fm")
                    template = template_fm(trait, reverse)
                elif ster == 'f':
                    print("template_ff")
                    template = template_ff(trait, reverse) 
                else:
                    raise Exception("what??")     
            else:
                raise Exception("what??")         

        if ttype == 2:
            #This type is about guessing the gender given the trait
            if ster == 'm':
                print("template_male_ster")
                template = template_male_ster(trait, reverse)
            elif ster == 'f':
                print("template_female_ster")
                template = template_female_ster(trait, reverse)
            else:
                raise Exception("what??")                                            

        
        
        male_ster_def = get_hint_male_ster(trait)
        female_ster_def = get_hint_female_ster(trait)
        editor = Editor()
        examples = editor.template(template).data
        examples = expand_examples(examples, 
                                   refs_multiple, refs_single, 
                                   males_ref, females_ref,
                                   male_ster_def, female_ster_def)
        json_data = to_json(examples)
        fname = "data/temp.jsonl"
        # print('gggggggggggggggggg')
        with open(fname, 'w') as f:
            for anli_ip in json_data:
                f.write(json.dumps(anli_ip) + "\n")
        pred_labels, confidence = evaluate(fname, 32, model_dir, True)
        confidences[trait] = confidence
        print(f"Trait: {trait}")
        print(f"Examples:\n{examples[0]}\n-------\n{examples[1]}\n------")
        results = infer(pred_labels, confidence)
        print("================================")
        combined_data[trait] = results
    return combined_data, np.array(confidence)    