from pytorch_transformers import BertTokenizer, BertConfig, BertForMultipleChoice
from anli.data_processors import AnliProcessor, mc_examples_to_data_loader# get_examples_from_file
import json
import torch
from tqdm import tqdm
import numpy as np
# import data_lists
from checklist.editor import Editor
import matplotlib.pyplot as plt





def get_template_simple(trait, reverse=False):
    templates = {
    'aggressive' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",        
    
    'leader' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'logical' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'emotionally stable' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'ambitious' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'independent' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 
    
    'skilled in business' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",  
    
    'hides emotions' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",     
   
    'dominant' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'math_science' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'competitive' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'uses_harsh_language' : """There is a <<ref_single>>.
                The <<ref_single>> <<male_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",     
    }
    t = [x.strip() for x in templates[trait].split('\n')]
    if reverse:
        t[-2], t[-1] = t[-1], t[-2]
    return "\n".join(t)


def get_template_simple_fem(trait, reverse=False):
    templates = {
    'aggressive' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",        
    
    'leader' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'logical' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'emotionally stable' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'ambitious' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",
    
    'independent' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 
    
    'skilled in business' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",  
    
    'hides emotions' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",     
   
    'dominant' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'math_science' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'competitive' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""", 

    'uses_harsh_language' : """There is a <<ref_single>>.
                The <<ref_single>> <<female_ster_def>>.
                The <<ref_single>> is a <<males_ref>>.
                The <<ref_single>> is a <<females_ref>>.""",     
    }
    t = [x.strip() for x in templates[trait].split('\n')]
    if reverse:
        t[-2], t[-1] = t[-1], t[-2]
    return "\n".join(t)



def expand_examples(examples, refs_multiple, refs_single,
                       males_ref, females_ref, male_ster_def, female_ster_def):
    expanded_examples = []
    for example in examples:
        for rm, rs in zip(refs_multiple, refs_single):
            for mr, fr in zip(males_ref, females_ref):
                for msd, fsd in zip(male_ster_def, female_ster_def):
                    e = example.replace("<<ref_multiple>>", rm)
                    e = e.replace("<<ref_single>>", rs)
                    e = e.replace("<<males_ref>>", mr)
                    e = e.replace("<<females_ref>>", fr)
                    e = e.replace("<<male_ster_def>>", msd)
                    e = e.replace("<<female_ster_def>>", fsd)
                    expanded_examples.append(e)
    return expanded_examples            


def get_lists():
    traits = [
     'aggressive',
     'dominant',
     'math_science',
     'competitive',
     'uses_harsh_language',
     'leader',
     'logical',
     'emotionally stable',
     'ambitious',
     'independent',
     'skilled in business',
     'hides emotions',
    ]

    refs_multiple = [
        'students',
        'people',
        'employees',
    ]

    refs_single = [
        'student',
        'person',
        'employee',
    ]

    males_ref = [
        'boy',
        'man',
        'male',
    ]

    females_ref = [
        'girl',
        'woman',
        'female',
    ]    

    return traits, refs_multiple, refs_single, males_ref, females_ref